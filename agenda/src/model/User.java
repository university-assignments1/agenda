package model;

import java.sql.Date;
import java.util.Collection;

public class User {

	private String username, firstname, lastname;
	private EmailAddress email;
	private Collection<Meeting> OwnedMeetings;
	private Collection<Meeting> ParticipateMeetings;
	
	
	
	public User(String uname, String email, String fn, String ln,
				Collection<Meeting> owm, Collection<Meeting> prm) {
		this.username = uname;
		this.email = new EmailAddress(email);
		this.firstname = fn;
		this.lastname = ln;
		
		this.OwnedMeetings = owm;
		this.ParticipateMeetings = prm;
	}
	
	public User(String uname, String email, String fn, String ln) {
		this.username = uname;
		this.firstname = fn;
		this.lastname = ln;
		this.email = new EmailAddress(email);
	}

	public void CreateMeeting(Date s, Date e) {
		OwnedMeetings.add(new Meeting(this, s, e));
		
		// TODO: Sql -- create meeting.
	}
	
	public void AddToMeeting(Meeting m) {
		ParticipateMeetings.add(m);
		m.AddUserToMeeting(this);
		
		// TODO: Sql -- add user to meeting.
	}
	
	
	
	//===== Getters - Setters =====
	public Collection<Meeting> getOwnedMeetings() {return OwnedMeetings;}
	public Collection<Meeting> getParticipateMeetings() {return ParticipateMeetings;}
	public String getEmail() {return email.getEmail();}
	public String getUsername() {return username;}
	public String getFirstname() {return firstname;}
	public String getLastname() {return lastname;}
	//===== Getters - Setters =====

}
