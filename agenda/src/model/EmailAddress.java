package model;

public class EmailAddress {
	
	private String domain, address, suffix;

	public EmailAddress(String email) {
		SetEmail(email);
	}
	
	public boolean ChangeEmail(String new_email) {
		return SetEmail(new_email);
	}
	
	
	//===== Getters - Setters =====
	private boolean SetEmail(String e) {
		// TODO: Check for valid email format and set domain, address.
		return true;
	}
	
	public String getEmail() {
		return this.getAddress() + "@" + this.getDomain() + "." + this.getSuffix();
	}
	
	public String getDomain() {return domain;}
	public String getAddress() {return address;}
	public String getSuffix() {return suffix;}
	//===== Getters - Setters =====
	
}
