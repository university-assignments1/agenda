package model;

import java.sql.Date;
import java.util.Collection;

public class Meeting {

	private int meeting_id;
	private Date start_date, end_date;
	private User Creator;
	private Collection<User> Participants;
	
	
	
	public Meeting(User c, Date sd, Date ed) {
		this.Creator = c;
		this.start_date = sd;
		this.end_date = ed;
	}

	public void AddUserToMeeting(User u) {
		Participants.add(u);
	}
	
	
	//===== Getters - Setters =====
	public int getMeeting_id() {return meeting_id;}
	public Date getStart_date() {return start_date;}
	public Date getEnd_date() {return end_date;}
	public User getCreator() {return Creator;}
	public Collection<User> getParticipants() {return Participants;}
	//===== Getters - Setters =====	
	
}
