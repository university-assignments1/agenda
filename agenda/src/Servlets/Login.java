package Servlets;

import static constants.Errors.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import model.Meeting;
import model.User;

@WebServlet("/Login")
public class Login extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private DataSource ds = null;

	public Login() {
		super();
	}

	public void init() throws ServletException {
		try {

			InitialContext ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/LiveDataSource");
		} catch (Exception e) {
			throw new ServletException(e.toString());
		}
	}

	private String HashPass(String passwordNoHash, String salt)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		
		MessageDigest md5 = MessageDigest.getInstance("MD5");

		byte[] saltb = salt.getBytes("UTF8");

		md5.update(saltb);
		md5.update(passwordNoHash.getBytes("UTF8"));

		byte[] hashed_pass = md5.digest();

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < hashed_pass.length; i++)
			sb.append(Integer.toString((hashed_pass[i] & 0xff) + 0x100, 16).substring(1));

		return sb.toString();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		Object obj = null;
		String email = request.getParameter("email");

		// Το καθαρό password (χωρίς salt) το οποίο έχει δώσει ο χρήστης.
		String password = request.getParameter("password");

		if (email.isEmpty() || password.isEmpty()) {
			response.getWriter().append(EMPTY_LOGIN);
		}

		Connection c = null;
		PreparedStatement prep_stmt = null;
		ResultSet rs = null;

		try {
			c = ds.getConnection();

			System.out.println("Login");
			
			String sql = "SELECT * FROM users WHERE email=?";

			prep_stmt = c.prepareStatement(sql);
			prep_stmt.setString(1, email);

			rs = prep_stmt.executeQuery();

			if (rs.next()) {

				String password_hashed = null;

				try {
					password_hashed = HashPass(password, rs.getString("salt"));
				} catch (NoSuchAlgorithmException e) {
					e.printStackTrace();
				}

				if (password_hashed != null && password_hashed.equals(rs.getString("password"))) {
					System.out.println("Login successfully");

					HttpSession session = request.getSession();

					String jsp_name = null;

					// TODO: Get Meetins Owned and Participate from database.
					obj = new User(rs.getString("username"), rs.getString("email"), rs.getString("firstname"), rs.getString("lastname"));

					jsp_name = "calendar.jsp";

					session.setAttribute("obj", obj);

					response.sendRedirect(jsp_name);

				} else {
					System.out.println("wrong password for email " + email);
					response.getWriter().append(INVALID_LOGIN);
				}

			} else {
				System.out.println("wrong email");
				response.getWriter().append("Error on email or password");
			}

		}

		catch (SQLException ex) {
			Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
