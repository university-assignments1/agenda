package Servlets;

import java.awt.List;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.apache.catalina.tribes.util.Arrays;
import org.postgresql.jdbc2.TimestampUtils;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.RegularExpression;
import com.sun.tools.javac.code.Attribute.Array;

import model.User;
import static constants.Queries.*;


@WebServlet("/Calendar/*")
public class Calendar extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private DataSource ds = null;   

    public Calendar() {
        super();
    }
    
    public void init() throws ServletException {
		try {

			InitialContext ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/LiveDataSource");
		} catch (Exception e) {
			throw new ServletException(e.toString());
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		String action = request.getPathInfo();
		
		HttpSession s = request.getSession(false);
		User usr = (User) s.getAttribute("obj");
		
		String title = request.getParameter("title");
		String note = request.getParameter("note");
		
		String day = request.getParameter("day");
		String month = request.getParameter("month");
		String year = request.getParameter("year");
		
		String allday = request.getParameter("allday");
		String start_time = request.getParameter("start");
		String end_time = request.getParameter("end");
		
		String d = year + '-' + month + '-' + day;
		
		Connection c = null;
		PreparedStatement prep_stmt = null;
		ResultSet rs = null;
		
		try {
			c = ds.getConnection();
			
			switch (action) {
			case "/add_meeting":
		
				System.out.println("Add meeting");
				
				String[] mentions = request.getParameterValues("mentions");
				
				prep_stmt = c.prepareStatement(ADD_MEETING);
				
				if (allday.equals("on")) {
					start_time = "00:00:00";
					end_time = "00:00:00";
				} else {
					start_time += ":00";
					end_time += ":00";
				}
				
				prep_stmt.setDate(1, Date.valueOf(d));
				prep_stmt.setTime(2, Time.valueOf(start_time));
				prep_stmt.setTime(3, Time.valueOf(end_time));
				prep_stmt.setString(4, usr.getUsername());
				prep_stmt.setString(5, title);
				prep_stmt.setString(6, note);
				
				rs = prep_stmt.executeQuery();
				
				rs.next();
				int meeting_id = rs.getInt(1);
				
				if (mentions != null) {
					
					for (String i : mentions) {
						
						if (i.equals(usr.getUsername()))
							continue;
						
						prep_stmt = c.prepareStatement(MENTION_USER);
						
						prep_stmt.setString(1, i);
						prep_stmt.setInt(2, meeting_id);
						
						prep_stmt.executeUpdate();
					}
				}
				
				break;
				
			case "/remove_meeting":
				
				System.out.println("Remove meeting");
				
				prep_stmt = c.prepareStatement(DELETE_MEETING);
				
				prep_stmt.setString(1, usr.getUsername());
				prep_stmt.setString(2, title);
				prep_stmt.setDate(3, Date.valueOf(d));
				
				prep_stmt.executeUpdate();
				
				break;
			
			case "/check_user":
				
				System.out.println("Check user");
				
				prep_stmt = c.prepareStatement(CHECK_USER);
				
				prep_stmt.setString(1, request.getParameter("username"));
				
				rs = prep_stmt.executeQuery();
				
				if (!rs.next()) {
					response.getWriter().append("ERROR");
				}
				else {
					response.getWriter().append("OK");
				}
				
				break;
			
			case "/update_meeting":
				
				System.out.println("Update meeting");
				
				prep_stmt = c.prepareStatement(UPDATE_MEETING);
				
				prep_stmt.setString(1, request.getParameter("note"));
				prep_stmt.setString(2, request.getParameter("title"));
				
				prep_stmt.executeUpdate();
				
				break;
				
			case "/logout":
				s.invalidate();
				response.sendRedirect("../index.html");
				break;

			default:
				response.getWriter().append("Unknown action: " + action);
				break;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		doGet(request, response);
	}

}
