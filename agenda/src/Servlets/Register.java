package Servlets;

import static constants.Errors.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private DataSource ds = null;

	public Register() {
		super();
	}

	public void init() throws ServletException {

		try {
			InitialContext ctx = new InitialContext();
			ds = (DataSource) ctx.lookup("java:comp/env/jdbc/LiveDataSource");
		} catch (Exception e) {
			throw new ServletException(e.toString());
		}
	}

	private String HashPass(String passwordNoHash, String salt)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {

		MessageDigest md5 = MessageDigest.getInstance("MD5");

		byte[] saltb = salt.getBytes("UTF8");

		md5.update(saltb);
		md5.update(passwordNoHash.getBytes("UTF8"));

		byte[] hashed_pass = md5.digest();

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < hashed_pass.length; i++)
			sb.append(Integer.toString((hashed_pass[i] & 0xff) + 0x100, 16).substring(1));

		return sb.toString();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		
		Connection c = null;

		try {

			c = ds.getConnection();

			System.out.println("Opened database successfully");
			
			String firstname = request.getParameter("firstname");
			String lastname = request.getParameter("lastname");
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			String email = request.getParameter("email");

			// ελεγχος για το εαν εχει εγγραφει ηδη ο χρηστης
			PreparedStatement ps1 = c.prepareStatement("SELECT username FROM users WHERE username=?");
			ps1.setString(1, username);
			ResultSet rs2 = ps1.executeQuery();

			// εαν ο χρηστης δεν υπάρχει
			if (!rs2.next()) {
				
				PreparedStatement ps3 = c.prepareStatement("INSERT INTO users VALUES (?,?,?,?,?,?)");
				
				byte[] array = new byte[7]; // length is bounded by 7
			    new Random().nextBytes(array);
			    String salt = new String(array, Charset.forName("UTF-8"));
			    
			    String password_hased = HashPass(password, salt);
			    
				ps3.setString(1, username);
				ps3.setString(2, firstname);
				ps3.setString(3, lastname);
				ps3.setString(4, email);
				ps3.setString(5, password_hased);
				ps3.setString(6, salt);
				ps3.executeUpdate();
				ps3.close();
				
				response.getWriter().append("Η εγγραφή ολοκληρώθηκε.");
				
			} else {
				
				response.getWriter().append("Ο χρήστης υπάρχει!");
			}

			ps1.close();

		} catch (SQLException | NoSuchAlgorithmException e) {
			System.out.println(e.getMessage());
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
