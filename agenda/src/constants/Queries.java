package constants;

public final class Queries {

	public static final String ADD_MEETING=	"INSERT INTO meetings (Date, Start_Time, End_time, Creator, Title, Note)\n" +
											"VALUES (?, ?, ?, ?, ?, ?)" +
											"RETURNING meeting_id;";
	
	public static final String GET_USER_MEETINGS=	"SELECT *\n" + 
													"FROM meeting_info NATURAL JOIN meetings\n" + 
													"WHERE username=?;";
	
	public static final String DELETE_MEETING=	"DELETE FROM meetings\n" + 
												"WHERE creator=? AND title=? AND date=?;";
	
	public static final String CHECK_USER=	"SELECT *\n" +
											"FROM users\n" +
											"WHERE username=?";
	
	public static final String MENTION_USER=	"INSERT INTO meeting_info (Username, Meeting_id, Accept)\n" +
												"VALUES (?,?,False)";
	
	public static final String UPDATE_MEETING=	"UPDATE meetings\n" +
												"SET note=?\n" +
												"WHERE title=?;";
	
	public static final String GET_INVITED_USERS=	"SELECT *\n" +
													"FROM meeting_info\n" +
													"WHERE meeting_id=?;";
}
