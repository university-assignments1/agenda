<%@page import="java.lang.reflect.Array"%>
<%@page import="java.awt.List"%>
<%@page import="model.User"%>
<%@page import="javax.servlet.http.HttpSession"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" %>
<%@page import="java.sql.Date" %>
<%@page import="java.time.LocalDate" %>
<%@page import="java.sql.SQLException" %>
<%@page import="java.sql.Connection" %>
<%@page import="java.sql.PreparedStatement" %>
<%@page import="javax.sql.DataSource" %>
<%@page import="java.sql.ResultSet" %>
<%@page import="javax.naming.InitialContext" %>
<%@page import="javax.naming.NamingException" %>
<%@page import="static constants.Queries.*" %>
<%@page import="java.util.ArrayList" %>
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="UTF-8">
<title>Calendar in ReactJs</title>


<link rel='stylesheet'
	href='https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css'>

<link rel="stylesheet" href="css/calendar.css">


</head>

<head>
<script src="https://use.fontawesome.com/484df5253e.js"></script>
<script src='https://code.jquery.com/jquery-2.2.4.min.js'></script>
<script src='https://npmcdn.com/react@15.3.0/dist/react.min.js'></script>
<script src='https://npmcdn.com/react-dom@15.3.0/dist/react-dom.min.js'></script>
</head>
<body>

	<%
		response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
		response.setHeader("Pragma", "no-cache");

		HttpSession s = request.getSession(false);
		User usr = null;

		try {
			usr = (User) s.getAttribute("obj");

		} catch (Exception ex) {

			request.setAttribute("message", "Πρέπει να κάνεις login");
			request.setAttribute("forward", "index.html");
			request.getRequestDispatcher("message_page.jsp").forward(request, response);
			throw new javax.servlet.jsp.SkipPageException();
		}

		if (usr == null) {
			request.setAttribute("message", "Πρέπει να κάνεις login");
			request.setAttribute("forward", "index.html");
			request.getRequestDispatcher("message_page.jsp").forward(request, response);
			throw new javax.servlet.jsp.SkipPageException();
		}
	%>


	<!-- Setup calendar for logged user -->
	<script>
		var madeName = '<%=usr.getFirstname()%>';
		var madeInfo = '<%=usr.getLastname()%>';
	</script>



	<div id="app"></div>

	<script src="js/calendar.js"></script>

	<script type="application/javascript">
    
    var Calendar = React.createClass({ displayName: "Calendar",
    	  getInitialState: function () {return this.generateCalendar();},
    	  generateCalendar: function () {
    	    var today = new Date();
    	    var present = new Date();
    	    var month = {};
    	    var entries = [];
    	    var defaultColor = { color: "#2980b9" };
    	    var color1 = { color: "#DB1B1B" };
    	    var color2 = { color: "#8BB929" };
    	    var color3 = { color: "#E4F111" };
    	    var color4 = { color: "#8129B9" };
    	    var color5 = { color: "#666666" };
    	    var file = {};
    	    month = new Month(today.getFullYear(), today.getMonth() + 1, month);
    	    return { dates: month, today: today, entry: '+', present: present, entries: entries, dColor: defaultColor, color1: color1, color2: color2, color3: color3, color4: color4, color5: color5, file: file };
    	  },
    	  update: function (direction) {
    	    var month = {};
    	    if (direction == "left") {
    	      month = new Month(this.state.dates.date.getFullYear(), this.state.dates.date.getMonth(), month);
    	    } else {
    	      month = new Month(this.state.dates.date.getFullYear(), this.state.dates.date.getMonth() + 2, month);
    	    }
    	    this.state.currDay = "";
    	    this.state.currMonth = "";
    	    this.state.currYear = "";
    	    $(".float").removeClass('rotate');
    	    return this.setState({ dates: month });
    	  },
    	  selectedDay: function (day) {
    	    this.state.warning = "";
    	    var selected_day = new Date();
    	    selected_day.setDate(day);
    	    var currentMonth = this.state.dates.nameofmonth;
    	    var currentMonthN = this.state.dates.numberofmonth;
    	    var currentYear = this.state.dates.date.getFullYear();
    	    return this.setState({ today: selected_day, currDay: day, currMonth: currentMonth, currYear: currentYear, currMonthN: currentMonthN });
    	  },
    	  returnPresent: function () {
    	    if ($(".float").hasClass('rotate')) {
    	      $(".float").removeClass('rotate');
    	      $("#add_entry").addClass('animated slideOutDown');
    	      window.setTimeout(function () {
    	        $("#add_entry").css('display', 'none');
    	      }, 500);
    	      $("#entry_name").val("");
    	    }
    	    var month = {};
    	    var today = new Date();
    	    month = new Month(today.getFullYear(), today.getMonth() + 1, month);
    	    this.state.currDay = "";
    	    this.state.currMonth = "";
    	    this.state.currYear = "";
    	    $(".float").removeClass('rotate');
    	    return this.setState({ dates: month, today: today });
    	  },
    	  addEntry: function (day) {
    	    if (this.state.currDay) {
    	      if ($(".float").hasClass('rotate')) {
    	        $(".float").removeClass('rotate');
    	        $(".entry").css('background', 'none');
    	        $("#open_entry").addClass('animated slideOutDown');
    	        $("#add_entry").addClass('animated slideOutDown');
    	        window.setTimeout(function () {
    	          $("#add_entry").css('display', 'none');
    	          $("#open_entry").css('display', 'none');
    	        }, 700);
    	        $("#entry_name").val("");
    	        $("#all-day").prop('checked', false); // unchecks checkbox
    	        $("#not-all-day").css('display', 'block');
    	        $(".enter_hour").val("");
    	        $("#entry_location").val("");
    	        $("#entry_note").val("");
    	        // reset entry colors
    	        var resColor = new resetColors();
    	        return this.setState(resColor);
    	      } else {
    	        $(".float").addClass('rotate');
    	        $("#add_entry").removeClass('animated slideOutDown');
    	        $("#add_entry").addClass('animated slideInUp');
    	        $("#add_entry").css('display', 'block');
    	        window.setTimeout(function () {
    	          $("#entry_name").focus();
    	        }, 700);

    	      }
    	    } else {
    	      return this.setState({ warning: "Select a day to make an entry!" });
    	    }
    	  },
    	  saveEntry: function (year, month, day, send_flag = true, list_m = null) {
    	    var entryName = $("#entry_name").val();
    	    if ($.trim(entryName).length > 0) {
    	      var entryTime = new Date();
    	      var entryDate = { year, month, day };
    	      $(".duration").css('background', 'none');
    	      if ($("#all-day").is(':checked')) {
    	        var entryDuration = "All day";
    	      } else if ($("#start_hour").val().length && $("#end_hour").val().length >= 0) {
    	        var entryDuration = addZero($(".enter_hour").val());
    	      } else {
    	        $(".duration").css('background', '#F7E8E8');
    	        return 0;
    	      }
    	      if ($("#entry_location").val()) {
    	        var entryLocation = $("#entry_location").val();
    	      } else {var entryLocation = "";}
    	      if ($("#entry_note").val()) {
    	        var entryNote = $("#entry_note").val();
    	      } else {var entryNote = "";}

    	      var entryImg = this.state.file;
    	      var entryColor = this.state.dColor;
    	      
    	      
    	      var entr_title = $('#entry_name').val();
    	      var entr_allday = ($("#all-day").is(':checked') ? "on" : "off");
    	      var entr_start = $('#start_hour').val();
    	      var entr_end = $('#end_hour').val();
    	      

		   	  // Create the list of mentions
    	      if (list_m == null){
        	      var list_mentions = [];
        	      var tmp = document.getElementById("mentions_list").getElementsByTagName("li");
    	    	  
    	    	  var i;
    			  for(i = 0; i < tmp.length; i++) {
    				  list_mentions.push(tmp[i].innerHTML);
    			  }  
    	      } else {
    	    	  list_mentions = list_m;
    	      }
    	      			  
			  var entry = { entryName, entryDate, entryTime, entryDuration, entryLocation, entryNote, entryColor, entryImg, list_mentions };
			  this.state.entries.splice(0, 0, entry);
			  
    	      // clean and close entry page
    	      $(".float").removeClass('rotate');
    	      $("#add_entry").addClass('animated slideOutDown');
    	      window.setTimeout(function () {
    	        $("#add_entry").css('display', 'none');
    	      }, 700);
    	      $("#entry_name").val("");
    	      $("#all-day").prop('checked', false); // unchecks checkbox
    	      $("#not-all-day").css('display', 'block');
    	      $("#start_hour").val("");
    	      $("#end_hour").val("");
    	      $("#entry_location").val("");
    	      $("#entry_note").val("");
    	      // reset entry colors
    	      var resColor = new resetColors();
    	      // Clear mentions list
    	      $("#mentions_list")[0].innerHTML = '';
    	      
    	      if (send_flag) {
    	    	  $.ajax({
        	          type: 'post',
        	          url: 'Calendar/add_meeting',
        	          datatype: "text",
        	          traditional: true,
        	          data: {
        	          	title: entr_title,
        	          	allday: entr_allday,
        	          	note: entryNote,
        	          	day: cal_app.state.currDay,
        	          	month: cal_app.state.currMonthN + 1,
        	          	year: cal_app.state.currYear,
        	          	start: entr_start,
        	          	end: entr_end,
        	          	mentions: list_mentions
        	          },
        	          success: function () {}
        	      });  
    	      }
    	      
    	      return this.setState({ entries: this.state.entries }), this.setState(resColor);
    	    }
    	  },
    	  deleteEntry: function (e) {
    	    this.state.entries.splice(e, 1);
    	    $(".float").removeClass('rotate');
    	    $("#open_entry").addClass('animated slideOutDown');
    	    $("#add_entry").addClass('animated slideOutDown');
    	    window.setTimeout(function () {
    	      $("#add_entry").css('display', 'none');
    	      $("#open_entry").css('display', 'none');
    	    }, 700);
    	    $(".entry").css('background', 'none');
    	    $("#entry_name").val("");
    	    $("#all-day").prop('checked', false); // unchecks checkbox
    	    $("#not-all-day").css('display', 'block');
    	    $(".enter_hour").val("");
    	    $("#entry_location").val("");
    	    $("#entry_note").val("");
    	    var resColor = new resetColors();
    	    
    	    var entr_title = $('.entry_event')[0].innerHTML;
    	    
    	    $.ajax({
	 	          type: 'post',
	 	          url: 'Calendar/remove_meeting',
	 	          datatype: "text",
	 	          data: {
	 	          	title: entr_title,
	 	          	day: cal_app.state.currDay,
	 	          	month: cal_app.state.currMonthN + 1,
	 	          	year: cal_app.state.currYear
	 	          },
	 	          success: function () {}
  	      	 });  
    	    
    	    return this.setState({ entries: this.state.entries }), this.setState(resColor);
    	  },
    	  openEntry: function (entry, e) {
    		  
    	    if ($(".float").hasClass('rotate')) {
    	      $(".float").removeClass('rotate');
    	      $("#open_entry").addClass('animated slideOutDown');
    	      window.setTimeout(function () {
    	        $("#open_entry").css('display', 'none');
    	      }, 700);
    	      $(".entry").css('background', 'none');
    	      $("#" + e).css('background', 'none');
    	      
    	      $("#invited_users").innerHTML = '';
    	      
    	    } else {
    	      window.setTimeout(function () {
    	        $("#open_entry").removeClass('animated slideOutDown');
    	        $("#open_entry").addClass('animated slideInUp');
    	        $("#open_entry").css('display', 'block');
    	      }, 50);
    	      $(".float").addClass('rotate');
    	      $("#" + e).css('background', '#F1F1F1');
    	          	      
    	      return this.setState({ openEntry: entry });
    	    }
    	    
    	  },
    	  setColor: function (color, state) {
    	    switch (state) {
    	      case 'color1':
    	        var changeColor = { color: this.state.dColor.color };
    	        var defColor = { color: color.color };
    	        return this.setState({ dColor: defColor, color1: changeColor });
    	        break;
    	      case 'color2':
    	        var changeColor = { color: this.state.dColor.color };
    	        var defColor = { color: color.color };
    	        return this.setState({ dColor: defColor, color2: changeColor });
    	        break;
    	      case 'color3':
    	        var changeColor = { color: this.state.dColor.color };
    	        var defColor = { color: color.color };
    	        return this.setState({ dColor: defColor, color3: changeColor });
    	        break;
    	      case 'color4':
    	        var changeColor = { color: this.state.dColor.color };
    	        var defColor = { color: color.color };
    	        return this.setState({ dColor: defColor, color4: changeColor });
    	        break;
    	      case 'color5':
    	        var changeColor = { color: this.state.dColor.color };
    	        var defColor = { color: color.color };
    	        return this.setState({ dColor: defColor, color5: changeColor });
    	        break;}

    	  },
    	  handleImage: function (e) {
    	    e.preventDefault();
    	    let reader = new FileReader();
    	    let file = e.target.files[0];
    	    if (file) {
    	      reader.onloadend = () => {
    	        var readerResult = reader.result;
    	        var img = { file, readerResult };
    	        this.setState({ file: img });
    	      };
    	      reader.readAsDataURL(file);
    	    } else {
    	      var img = {};
    	      this.setState({ file: img });
    	    }
    	  },
    	  openMenu: function () {
    	    $("#menu").css('display', 'block');
    	    $("#menu-content").addClass('animated slideInLeft');
    	    $("#menu-content").css('display', 'block');
    	  },
    	  render: function () {
    	    var calendar = [];
    	    for (var property in this.state.dates.calendar) {
    	      calendar.push(this.state.dates.calendar[property]);
    	    }
    	    var weekdays = Object.keys(this.state.dates.calendar);
    	    var done = false;
    	    var count = 0;
    	    var daysBetween = '';
    	    if (this.state.openEntry) {
    	      var selectdDate = new Date(this.state.openEntry.entryDate.year, this.state.openEntry.entryDate.month, this.state.openEntry.entryDate.day);
    	      if (selectdDate > this.state.present) {
    	        daysBetween = Date.daysBetween(this.state.present, selectdDate);
    	        if (daysBetween == 1) {daysBetween = "Tomorrow";} else {daysBetween = daysBetween + " days to go";}
    	      } else if (selectdDate < this.state.present) {
    	        daysBetween = Date.daysBetween(selectdDate, this.state.present);
    	        if (daysBetween == 1) {daysBetween = "Yesterday";} else {daysBetween = daysBetween + " days ago";}
    	      }
    	      if (this.state.present.getDate() === this.state.openEntry.entryDate.day && this.state.present.getMonth() === this.state.openEntry.entryDate.month && this.state.present.getFullYear() === this.state.openEntry.entryDate.year) {
    	        daysBetween = "Today";
    	      }
    	    }
    	    return (
    	      React.createElement("div", null,
    	      React.createElement("div", { id: "calendar" },
    	      React.createElement("div", { id: "menu" },
    	      React.createElement("div", { id: "menu-content" },
    	      React.createElement("div", { className: "madeBy" },
    	   	  React.createElement("form", { action: "Calendar/logout", method: "post" },
    	      React.createElement("button", { id: "logout_btn", type: "submit", className: "ripple" }, "Logout")),
    	      React.createElement("div", { className: "madeOverlay" },
    	      React.createElement("span", { id: "madeName" }, madeName ),
    	      React.createElement("span", { id: "madeInfo" }, madeInfo))),
    	      React.createElement("div", { className: "More" }, React.createElement("button", { id: "settings_btn", className: "ripple" }, "Settings")),
    	      React.createElement("div", { className: "More" }, React.createElement("button", { id: "profile_btn", className: "ripple" }, "Profile")),
    	      React.createElement("div", { className: "More" }, React.createElement("button", { id: "help_btn", className: "ripple" }, "Help"))),


    	      React.createElement("div", { id: "click-close" })),

    	      React.createElement("div", { id: "header" },
    	      React.createElement("i", { className: "fa fa-bars", "aria-hidden": "true", onClick: this.openMenu }),
    	      React.createElement("p", null, this.state.dates.nameofmonth, " ", this.state.dates.year),
    	      React.createElement("div", null, React.createElement("i", { onClick: this.returnPresent, className: "fa fa-calendar-o", "aria-hidden": "true" }, React.createElement("span", null, this.state.present.getDate()))),
    	      React.createElement("i", { className: "fa fa-search", "aria-hidden": "true" })),
    	      
    	      React.createElement("div", { id: "add_entry" },
		      React.createElement("form", { id: "add_meeting", action: "Calendar/add_meeting", method: "post" },
    	      React.createElement("div", { className: "enter_entry" },
    	      React.createElement("input", { type: "text", placeholder: "Enter title", id: "entry_name" }),
    	      React.createElement("span", { id: "save_entry", onClick: this.saveEntry.bind(null, this.state.currYear, this.state.currMonthN, this.state.currDay) }, "SAVE"))),

    	      React.createElement("div", { className: "entry_details" },
    	      React.createElement("div", null,
    	      React.createElement("div", { className: "entry_info first" },
    	      React.createElement("i", { className: "fa fa-image", "aria-hidden": "true" }),
    	      React.createElement("input", { type: "file", name: "entry-img", id: "entry-img", onChange: e => this.handleImage(e) }),
    	      React.createElement("label", { htmlFor: "entry-img", id: "for_img" }, React.createElement("span", { id: "file_name" }, "Choose an image")),
    	      React.createElement("span", { id: "remove_img" }, "Remove")),

    	      React.createElement("div", { className: "entry_info2 first second duration" },
    	      React.createElement("i", { className: "fa fa-clock-o", "aria-hidden": "true" }),
    	      React.createElement("input", { className: "toggle", type: "checkbox", name: "all-day", id: "all-day", value: "off" }),
    	      React.createElement("p", null, "All-day"),
    	      React.createElement("div", { id: "not-all-day" },
    	      React.createElement("p", { id: "select_hour" }, "Start time"),
    	      React.createElement("p", { id: "hour" }, React.createElement("input", { className: "enter_hour", type: "time", id: "start_hour", min: "0", max: "24" }), " h"),
			  React.createElement("p", { id: "select_hour" }, "End time"),
    	      React.createElement("p", { id: "hour" }, React.createElement("input", { className: "enter_hour", type: "time", id: "end_hour", min: "0", max: "24", }), " h"))	),


    	      React.createElement("div", { className: "entry_info2" },
    	      React.createElement("i", { className: "fa fa-plus-circle", "aria-hidden": "true" }),
    	      React.createElement("input", { type: "text", placeholder: "Invite", id: "entry_invite" }),
    	      React.createElement("button", { id: "mention" }, "Add")),
    	      
    	      React.createElement("ul", { id: "mentions_list" }),

    	      React.createElement("div", { className: "entry_info2" },
    	      React.createElement("i", { className: "fa fa-pencil", "aria-hidden": "true" }),
    	      React.createElement("textarea", { id: "entry_note", cols: "35", rows: "2", placeholder: "Add note" })),

    	      React.createElement("div", { className: "entry_info colors" },
    	      React.createElement("i", { className: "fa fa-circle", "aria-hidden": "true", id: "blue", style: this.state.dColor }),
    	      React.createElement("p", { id: "defColor" }, "Default color"),
    	      React.createElement("div", null,
    	      React.createElement("span", null, React.createElement("i", { onClick: this.setColor.bind(null, this.state.color1, "color1"), className: "fa fa-circle", "aria-hidden": "true", style: this.state.color1 })),
    	      React.createElement("span", null, React.createElement("i", { onClick: this.setColor.bind(null, this.state.color2, "color2"), className: "fa fa-circle", "aria-hidden": "true", style: this.state.color2 })),
    	      React.createElement("span", null, React.createElement("i", { onClick: this.setColor.bind(null, this.state.color3, "color3"), className: "fa fa-circle", "aria-hidden": "true", style: this.state.color3 })),
    	      React.createElement("span", null, React.createElement("i", { onClick: this.setColor.bind(null, this.state.color4, "color4"), className: "fa fa-circle", "aria-hidden": "true", style: this.state.color4 })),
    	      React.createElement("span", null, React.createElement("i", { onClick: this.setColor.bind(null, this.state.color5, "color5"), className: "fa fa-circle", "aria-hidden": "true", style: this.state.color5 }))))))),





    	      this.state.openEntry ?
    	      React.createElement("div", { id: "open_entry" },
    	      React.createElement("div", { className: "entry_img", style: { backgroundColor: this.state.openEntry.entryColor.color } },
    	      React.createElement("div", { className: "overlay" }, React.createElement("div", null,
			  React.createElement("button", { className: "ripple", id: "edit_btn", onClick: function() {
				  $('span').bind('dblclick',
				  	  function(){
				      	$(this).attr('contentEditable',true);
				  	  });
				  } }, "Edit"),
			  React.createElement("button", { className: "ripple", id: "save_btn", onClick: function() {
				  $.ajax({
		 	          type: 'post',
		 	          url: 'Calendar/update_meeting',
		 	          datatype: "text",
		 	          data: {
		 	          	title: document.getElementById("entry_title").innerHTML,
		 	          	note: document.getElementById("note").innerHTML
		 	          },
		 	          success: function () {}
	  	      	 });
			  } }, "Save" ),
    	      React.createElement("p", null,
    	      React.createElement("span", { id: "entry_title" }, this.state.openEntry.entryName),
    	      React.createElement("span", { id: "entry_times" }, daysBetween, " ", this.state.openEntry.entryDuration === "All day" ? "| All day" : "at " + this.state.openEntry.entryDuration + ":00")))),


    	      React.createElement("img", { src: this.state.openEntry.entryImg.readerResult, width: "400px", height: "300px" })),

    	      React.createElement("div", { className: "entry openedEntry" }, React.createElement("div", null,
    	      React.createElement("i", { className: "fa fa-map-marker", "aria-hidden": "true" }), " ", this.state.openEntry.entryLocation ? this.state.openEntry.entryLocation : React.createElement("span", null, "No location"))),

    	      React.createElement("div", { className: "entry openedEntry noteDiv" }, React.createElement("div", null,
    	      React.createElement("i", { className: "fa fa-pencil", "aria-hidden": "true" }), " ", this.state.openEntry.entryNote ? React.createElement("span", { id: "note" }, this.state.openEntry.entryNote) : React.createElement("span", { id: "note" }, "No comments"))),
    	      React.createElement("button", { id: "show_list", onClick: function() {
	  				var entry = cal_app.state.openEntry;
	  				for(i = 0; i < entry.list_mentions.length; i++) {
  						console.log(entry);
  						$("#invited_users").append("<li>" + entry.list_mentions[i] + "</li>");
  					}
	  				$("#show_list").hide();
    	      }}, "Show List"),
    	      React.createElement("ul", { id: "invited_users" })) :


    	      null,
    	      React.createElement("div", { id: "arrows" },
    	      React.createElement("i", { className: "fa fa-arrow-left", "aria-hidden": "true", onClick: this.update.bind(null, "left") }),
    	      React.createElement("i", { className: "fa fa-arrow-right", "aria-hidden": "true", onClick: this.update.bind(null, "right") })),

    	      React.createElement("div", { id: "dates" },
    	      calendar.map(function (week, i) {
    	        return (
    	          React.createElement("div", { key: i },
    	          React.createElement("p", { className: "weekname" }, weekdays[i].substring(0, 3)),
    	          React.createElement("ul", null,
    	          week.map(function (day, k) {
    	            var existEntry = {};
    	            {this.state.entries.map(function (entry, e) {
    	                if (entry.entryDate.day == day && entry.entryDate.month == this.state.dates.numberofmonth && entry.entryDate.year == this.state.dates.year) {
    	                  existEntry = { borderWidth: "2px", borderStyle: "solid", borderColor: "#8DBEDE" };
    	                  return;
    	                }
    	              }.bind(this));}
    	            return React.createElement("li", { className: day === this.state.today.getDate() ? "today" : null, key: k, style: existEntry, onClick: this.selectedDay.bind(null, day) }, day);
    	            
    	          }.bind(this)))));



    	      }.bind(this))),


    	      this.state.warning ?
    	      React.createElement("div", { className: "warning" },
    	      this.state.warning) :

    	      null,
    	      React.createElement("div", { id: "ignoreOverflow" }, React.createElement("button", { className: "float", onClick: this.addEntry.bind(null, this.state.today.getDate()) }, this.state.entry))),

    	      this.state.currDay ?
    	      React.createElement("div", { id: "entries" },
    	      React.createElement("div", { className: "contain_entries" },
    	      React.createElement("div", { id: "entries-header" },
    	      React.createElement("p", { className: "entryDay" }, this.state.currDay, " ", this.state.currMonth),
    	      this.state.present.getDate() === this.state.currDay && this.state.present.getMonth() === this.state.currMonthN && this.state.present.getFullYear() === this.state.currYear ? React.createElement("p", { className: "currday" }, "TODAY") : null),

    	      this.state.entries != '' ?
    	      React.createElement("div", null,
    	      this.state.entries.map(function (entry, e) {
    	        count++;
    	        var entryFromThisDate = entry.entryDate.day === this.state.currDay && entry.entryDate.month === this.state.currMonthN && entry.entryDate.year === this.state.currYear ? true : false;
    	        if (entryFromThisDate) {
    	          // prevent the "no-entries" div to appear in the next entries that
    				// are not from this day
    	          done = true;
    	          var style = { borderLeftColor: entry.entryColor.color, borderLeftWidth: "4px", borderLeftStyle: "solid" };
    	          return (
    	            React.createElement("div", { className: "entry", id: e, key: e },
    	            React.createElement("div", { style: style },
    	            React.createElement("div", { className: "entry_left", onClick: this.openEntry.bind(null, entry, e) },
    	            React.createElement("p", { className: "entry_event" }, entry.entryName),
    	            React.createElement("p", { className: "entry_time" }, entry.entryDuration === "All day" ? "All day" : entry.entryDuration + " h", " ", entry.entryLocation ? " | " + entry.entryLocation : null)),

    	            React.createElement("div", { className: "delete_entry" },
    	            React.createElement("i", { className: "fa fa-times", "aria-hidden": "true", onClick: this.deleteEntry.bind(null, e) })))));




    	        }
    	        if (count === this.state.entries.length) {
    	          if (!done) {
    	            done = true;
    	            return (
    	              React.createElement("div", { className: "no-entries", key: e },
    	              React.createElement("i", { className: "fa fa-calendar-check-o", "aria-hidden": "true" }),
    	              React.createElement("span", null, "No events planned for today")));


    	          }
    	        }
    	      }.bind(this))) :

    	      React.createElement("div", { className: "no-entries" },
    	      React.createElement("i", { className: "fa fa-calendar-check-o", "aria-hidden": "true" }),
    	      React.createElement("span", null, "No events planned for today")))) :




    	      null));


    	  } });

    	var cal_app = ReactDOM.render(React.createElement(Calendar, null), document.getElementById("app"));
    	
    	<%
	    	DataSource ds = null;
	    	try {
	        	InitialContext ctx = new InitialContext();
	        	ds = (DataSource)ctx.lookup("java:comp/env/jdbc/LiveDataSource");
	        } catch(NamingException e) {
	        	throw new ServletException(e.toString());
	        }
	    	
	    	Connection c = null;
			PreparedStatement prep_stmt = null;
			ResultSet rs = null;
			ResultSet rs2 = null;
			
			try {
				c = ds.getConnection();
				
				prep_stmt = c.prepareStatement(GET_USER_MEETINGS);
				
				prep_stmt.setString(1, usr.getUsername());
				rs = prep_stmt.executeQuery();
			} catch (SQLException e) {
				e.printStackTrace();
			}

			while(rs.next()) {
				LocalDate d = Date.valueOf(rs.getString("date")).toLocalDate();
				Integer id = rs.getInt("meeting_id");
				
				try {
					prep_stmt = c.prepareStatement(GET_INVITED_USERS);
					prep_stmt.setInt(1, id);
					
					rs2 = prep_stmt.executeQuery();
					
				} catch (SQLException e) {
					e.printStackTrace();
				}				
				
				ArrayList<String> list_m = new ArrayList<String>();
				
				while(rs2.next()) {
					list_m.add("'" + rs2.getString("username") + "'");
				}
		%>
	    	
	    	$("#entry_name").val("<%= rs.getString("title") %>");
	    	$("#all-day").prop('checked', true);
	    	$("#entry_note").val("<%= rs.getString("note") %>");
	    	
	    	cal_app.saveEntry(<%= d.getYear() %>, <%= d.getMonthValue() - 1 %>, <%= d.getDayOfMonth() %>, false, <%= list_m %>);
    	<%
			}
    	%>
    	
	</script>

</body>

</html>
