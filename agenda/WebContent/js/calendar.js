/*
	Problems i know that exist but i don't want to waste time with them (since it's just a playground):
	- When an entry title is big and needs a second line it overflows other stuff
	- the images takes a bit to load so dont rush opening the entry, no need to go into compressing
	- to remove an image from the input (while inserting an entry) just click the input and click cancel afterwards, the input keeps the file after inserting an entry, when you enter another entry the image will still be there for that new entry
 */
var daysOfWeek = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
		'Friday', 'Saturday' ];
var MonthNames = [ 'January', 'February', 'March', 'April', 'May', 'June',
		'July', 'August', 'September', 'October', 'November', 'December' ];

function Month(year, month, dates) {
	this.date = new Date(year, month, 0);
	this.numberofdays = this.date.getDate();
	this.numberofmonth = this.date.getMonth();
	this.nameofmonth = MonthNames[this.date.getMonth()];
	this.firstday = 1;
	this.year = this.date.getFullYear();
	this.calendar = generateCalendar(this.numberofdays, year, month - 1,
			this.firstday, dates);
}

function Date2Day(year, month, day) {
	return new Date(year, month, day).getDay();
}

function generateCalendar(numberofdays, year, month, day, dates) {
	var WEEKDAY = daysOfWeek[Date2Day(year, month, day)];
	if (WEEKDAY in dates) {
		dates[WEEKDAY].push(day);
	} else {
		dates[WEEKDAY] = [ day ];
	}
	day++;
	return day > numberofdays ? dates : generateCalendar(numberofdays, year,
			month, day, dates);
}
// to add a zero to the time when this is less than 10
function addZero(i) {
	if (i < 10) {
		i = "0" + i;
	}
	return i;
}

function resetColors() {
	var defaultColor = {
		color : "#2980b9"
	};
	var color1 = {
		color : "#DB1B1B"
	};
	var color2 = {
		color : "#8BB929"
	};
	var color3 = {
		color : "#E4F111"
	};
	var color4 = {
		color : "#8129B9"
	};
	var color5 = {
		color : "#666666"
	};
	return {
		dColor : defaultColor,
		color1 : color1,
		color2 : color2,
		color3 : color3,
		color4 : color4,
		color5 : color5
	};
}

Date.daysBetween = function(date1, date2) {
	var firstDate = new Date(date1.getFullYear(), date1.getMonth(), date1
			.getDate());
	var secondDate = new Date(date2.getFullYear(), date2.getMonth(), date2
			.getDate());
	var diference = (secondDate - firstDate) / 86400000;
	return Math.trunc(diference);
};

$(document).ready(
		function() {
	
			(function($, undefined) {
				$("#all-day").click(function() {
					if (this.checked) {
						$("#not-all-day").css('display', 'none');
						this.value = 'on';
					} else {
						$("#not-all-day").css('display', 'block');
						this.value = 'off';
					}
				});
	
				$("#click-close").click(
						function() {
							$("#menu-content").removeClass(
									'animated slideInLeft');
							$("#menu-content").addClass(
									'animated slideOutLeft');
							window.setTimeout(function() {
								$("#menu").css('display', 'none');
								$("#menu-content").css('display',
										'none');
								$("#menu-content").removeClass(
										'animated slideOutLeft');
							}, 750);
						});
	
				$("#entry-img")
						.bind(
								'change',
								function(e) {
									var label = this.nextElementSibling;
									var fileName = '';
									if (this.files) {
										fileName = e.target.value
												.split('\\').pop();
									} else {
										fileName = '';
									}
									if (fileName != '') {
										label.querySelector('span').innerHTML = fileName;
									} else {
										label.querySelector('span').innerHTML = "Choose an image";
									}
								});
	
				function hypot(x, y) {
					return Math.sqrt(x * x + y * y);
				}
	
				$("button")
						.each(
								function(el) {
									var self = $(this), html = self
											.html();
	
									self.html("").append(
											$('<div class="btn"/>')
													.html(html));
								})
						.append(
								$('<div class="ink-visual-container"/>')
										.append(
												$('<div class="ink-visual-static"/>')))
						.
	
						on(
								"mousedown touchstart",
								function(evt) {
									event.preventDefault();
	
									var self = $(this), container = self
											.find(".ink-visual-static",
													true).eq(0);
	
									if (!container.length)
										return;
	
									container.find(".ink-visual")
											.addClass("hide");
	
									var rect = this
											.getBoundingClientRect(), cRect = container[0]
											.getBoundingClientRect(), cx, cy, radius, diam;
	
									if (event.changedTouches) {
										cx = event.changedTouches[0].clientX;
										cy = event.changedTouches[0].clientY;
									} else {
										cx = event.clientX;
										cy = event.clientY;
									}
	
									if (self.is(".float")) {
										var rx = rect.width / 2, ry = rect.height / 2, br = (rx + ry) / 2, mx = rect.left
												+ rx, my = rect.top
												+ ry;
	
										radius = hypot(cx - mx, cy - my)
												+ br;
									}
									diam = radius * 2;
	
									var el = $(
											'<div class="ink-visual"/>')
											.width(diam)
											.height(diam)
											.css(
													"left",
													cx - cRect.left
															- radius)
											.css(
													"top",
													cy - cRect.top
															- radius)
											.
	
											on(
													"animationend webkitAnimationEnd oanimationend MSAnimationEnd",
													function() {
														var self2 = $(this);
	
														switch (event.animationName) {
														case "ink-visual-show":
															if (self
																	.is(":active"))
																self2
																		.addClass("shown");
															break;
	
														case "ink-visual-hide":
															self2
																	.remove();
															break;
														}
	
													}).
	
											on("touchend", function() {
												event.preventDefault();
											});
	
									container.append(el);
								});
	
				$(window).on(
						"mouseup touchend",
						function(evt) {
							$(".ink-visual-static").children(
									".ink-visual").addClass("hide");
						}).on("select selectstart", function(evt) {
					event.preventDefault();
					return false;
				});
			})(jQuery);
			
			$("#entry_invite").keypress(function() {
				$("#entry_invite").css("color", "black");
			});
			
			$("#mention").click(function() {
				
				var uname = $("#entry_invite").val();
				
				var tmp = document.getElementById("mentions_list").getElementsByTagName("li");
				var list = [];
				
				var i;
				for(i = 0; i < tmp.length; i++) {
					list.push(tmp[i].innerHTML);
				}
				
				if (uname && !list.includes(uname)) {

					$.ajax({
			 	          type: 'post',
			 	          url: 'Calendar/check_user',
			 	          cache:false,
			 	          datatype: "text",
			 	          data: {
			 	          	username: uname
			 	          },
			 	          success: function (data) {
			 	        	 
			 	        	 if (data === "ERROR" ) {
			 	        		$("#entry_invite").css("color", "red");
			 	        	 }
			 	        	 else {
			 	        		 $("#mentions_list").append("<li>" + uname + "</li>");
				 	        	 $("#entry_invite").val("");
			 	        	 }
			 	          }
		  	      	 });

				}
			});
	});
